private var objeto:GameObject;
private var shaderOriginal:Shader;
private var shaderNuevo:Shader;
public var camaraPlayer:Camera;
public var Zona:GameObject;
public var player:GameObject;
private var botonVistaCategoriasScript;

function Start(){
	objeto = transform.gameObject;
	//Se crean los shaders: normal y autoiluminado.
	shaderOriginal = Shader.Find("Transparent/Diffuse");
	shaderNuevo = Shader.Find("Self-Illumin/Diffuse");
	var botonVistaCategorias : GameObject = GameObject.Find("BotoneraVistaCategorias");
	botonVistaCategoriasScript = botonVistaCategorias.GetComponent("Boton Vista Categorias");
}
function OnMouseOver(){
	objeto.transform.GetComponent.<Renderer>().material.shader = shaderNuevo;
}
function OnMouseExit(){
	objeto.transform.GetComponent.<Renderer>().material.shader = shaderOriginal;
}

function OnMouseDown(){
  //Si la camara esta arriba...
    if(camaraPlayer.transform.position.y > 1){ 
	     // Teletranporto al player al lado del objeto 
         player.transform.position = Zona.transform.position;
         botonVistaCategoriasScript.CambiarATexturaGris();
         // Activo la animacion para que la camara baje
          camaraPlayer.GetComponent.<Animation>().CrossFade("Camara Buscador Zona Baja",0,1);
         //Le reactivo a la camara el giro hacia arriba y abajo
     	  camaraPlayer.GetComponent("MouseLook").enabled = true;
     	  player.GetComponent("CharacterMotor").enabled = true;
     	  player.GetComponent("MouseLook").enabled = true;
 
	}
	
}
