private var objeto:GameObject;
private var shaderOriginal:Shader;
private var shaderNuevo:Shader;
public var direccion:String;

function Start(){
	//print("COMIENZA");
	objeto = transform.gameObject;
	shaderOriginal = Shader.Find("Transparent/Diffuse");
	shaderNuevo = Shader.Find("Self-Illumin/Diffuse");
	//print(shaderOriginal.name);
}

function OnMouseOver(){
	//print("OVER");
	objeto.transform.GetComponent.<Renderer>().material.shader = shaderNuevo;
	//print(objeto.transform.renderer.material.shader.name);
}
function OnMouseExit(){
	//print("OUT");
	objeto.transform.GetComponent.<Renderer>().material.shader = shaderOriginal;
	//print(objeto.transform.renderer.material.shader.name);
}
function OnMouseDown(){
	//print("CLICK");
	if(Application.isWebPlayer){
		Application.ExternalEval("window.open('http://www.inixiavf.com/')");
	}
	else{
		Application.OpenURL(direccion);
	}
}