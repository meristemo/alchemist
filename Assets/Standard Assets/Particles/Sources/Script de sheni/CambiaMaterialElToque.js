private var objeto:GameObject;
private var shaderOriginal:Shader;
private var shaderNuevo:Shader;
public var direccion:String;

function Start(){
	objeto = transform.gameObject;
	shaderOriginal = Shader.Find("Transparent/Diffuse");
	shaderNuevo = Shader.Find("Self-Illumin/Diffuse");
}

function OnMouseOver(){
	objeto.transform.GetComponent.<Renderer>().material.shader = shaderNuevo;
}
function OnMouseExit(){
	objeto.transform.GetComponent.<Renderer>().material.shader = shaderOriginal;
}
function OnMouseDown(){
	print("CLICK");
	if(Application.isWebPlayer){
		Application.ExternalEval("window.open('http://visiteacapulco.com/es/cultura/El-toque-cultural')");
	}
	else{
		Application.OpenURL(direccion);
	}
}