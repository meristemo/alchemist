private var objeto:GameObject;
private var MouseLook:GameObject;
private var shaderOriginal:Shader;
private var shaderNuevo:Shader;
public var camaraPlayer:Camera;
//public var camaraBuscadorZona:Camera;
public var player:GameObject;
private var brilla: boolean = false;
public var texturaGris:Texture;
public var texturaNaranja:Texture;


function Start(){
	objeto = transform.gameObject;
	//Se crean los shaders: normal y autoiluminado.
	shaderOriginal = Shader.Find("Transparent/Diffuse");
	shaderNuevo = Shader.Find("Self-Illumin/Diffuse");
	camaraPlayer.GetComponent.<Camera>().enabled = true;
	//camaraBuscadorZona.camera.enabled = false;

}
function OnMouseOver(){
	//objeto.transform.renderer.material.shader = shaderNuevo;
	if (!brilla) {
	  	GetComponent.<Animation>().Play("Botonera Brillo Aumenta");
	  	brilla = true;
	}
}
function OnMouseExit(){
	//objeto.transform.renderer.material.shader = shaderOriginal;
	if (brilla) {
	  	GetComponent.<Animation>().Play("Botonera Brillo Disminuye");
	  	brilla = false;
	}
}

function OnMouseDown(){
  //Si la camara esta abajo...
    if(camaraPlayer.transform.position.y < 1){ 
        // Desactivo la camara del player
        //camaraPlayer.camera.enabled = false;
       	 // Activo la camara del buscador por zona
	     //camaraBuscadorZona.camera.enabled = true;
	     // Teletranporto al player al lado del objeto 
        // CAMBIAR LO QUE ESTA ENTRE PARENTESIS SEGUN EL LUGAR A DONDE SE DEBA IR.
         player.transform.position = Vector3(-8.722306,0.3546411,10.06405);
         //player.transform.rotate(0,-90,0, Space.Self);
         player.transform.rotation = Quaternion.Euler(0, -90, 0);
        // Activo la animacion para que la camara suba
          camaraPlayer.GetComponent.<Animation>().CrossFade("Camara Buscador Zona Sube",0.1);
    	//Le quito a la camara el giro hacia arriba y abajo
     	  camaraPlayer.GetComponent("MouseLook").enabled = false;
     	  player.GetComponent("CharacterMotor").enabled = false;
     	  player.GetComponent("MouseLook").enabled = false;
     	  GetComponent.<Renderer>().material.mainTexture = texturaNaranja;
 


	}
	
  //Si la camara esta arriba...	
  else{
  	//CambiarATexturaGris();
     /*
       //Le reactivo a la camara el giro hacia arriba y abajo
	    camaraPlayer.GetComponent("MouseLook").enabled = true;
	     player.GetComponent("CharacterMotor").enabled = true;
     	  player.GetComponent("MouseLook").enabled = true;
	   // Activo la camara del player
        //camaraPlayer.camera.enabled = true;
       // Desactivo la camara del buscador por zona
	   // camaraBuscadorZona.camera.enabled = false;
	    // Activo la animacion para que la camara baje
          camaraPlayer.animation.CrossFade("Camara Buscador Zona Baja",0.1);
          */
  }
}


function CambiarATexturaGris(){
	GetComponent.<Renderer>().material.mainTexture = texturaGris;
}

