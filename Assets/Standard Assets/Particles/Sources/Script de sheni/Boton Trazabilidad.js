private var objeto:GameObject;
public var ficha:GameObject;
//public var cerrar:GameObject;
private var colicionador:GameObject;
//private var shaderOriginal:Color;
//private var shaderNuevo:Shader;
//public var direccion:String;
private var brilla: boolean = false;
public var texturaGris:Texture;
public var texturaNaranja:Texture;

function Start(){
	colicionador = new GameObject.Find("ColicionadorDeFicha");

}

function OnMouseOver(){
	//objeto.transform.renderer.material.shader = shaderNuevo;
	  //objeto.transform.renderer.material.color = Color.white;
	  if (!brilla) {
	  	GetComponent.<Animation>().Play("Botonera Brillo Aumenta");
	  	brilla = true;
	  }
}
function OnMouseExit(){
	//objeto.transform.renderer.material.shader = shaderOriginal;
	//objeto.transform.renderer.material.color = shaderOriginal;
	if (brilla) {
	  	GetComponent.<Animation>().Play("Botonera Brillo Disminuye");
	  	brilla = false;
	}
}
function OnMouseDown(){
	ficha.GetComponent.<Animation>().Play("ApareceTrazabilidad");

    if(GetComponent.<Renderer>().material.mainTexture == texturaGris){ 
     	GetComponent.<Renderer>().material.mainTexture = texturaNaranja;
	}
    else{
		GetComponent.<Renderer>().material.mainTexture = texturaGris;
		ficha.GetComponent.<Animation>().Play("DesapareceTrazabilidad");
    }

    //cerrar.collider.enabled = true;
    //colicionador.collider.enabled = true;
	}