public var texturaGris:Texture;
public var texturaNaranja:Texture;
private var categoriasRenderer;
private var alimentosRenderer;
private var alimentosCollider;
private var bazarRenderer;
private var bazarCollider;
private var bebidasRenderer;
private var bebidasCollider;
private var carniceriaRenderer;
private var carniceriaCollider;
private var ferreteriaRenderer;
private var ferreteriaCollider;
private var lacteosRenderer;
private var lacteosCollider;
private var verduleriaRenderer;
private var verduleriaCollider;
 		
function Start(){
	//var categorias : GameObject = GameObject.Find("Categorias");
	//categoriasRenderer = categorias.GetComponent ("Activater");
	
	categoriasRenderer = GameObject.Find("Categorias").GetComponent(Renderer);
	
	alimentosRenderer = GameObject.Find("BotoneraAlimentos").GetComponent(Renderer);
	alimentosCollider = GameObject.Find("BotoneraAlimentos").GetComponent(Collider);
    bazarRenderer = GameObject.Find("BotoneraBazar").GetComponent(Renderer);
    bazarCollider = GameObject.Find("BotoneraBazar").GetComponent(Collider);
    bebidasRenderer = GameObject.Find("BotoneraBebidas").GetComponent(Renderer);
    bebidasCollider = GameObject.Find("BotoneraBebidas").GetComponent(Collider);
    carniceriaRenderer = GameObject.Find("BotoneraCarniceria").GetComponent(Renderer);
    carniceriaCollider = GameObject.Find("BotoneraCarniceria").GetComponent(Collider);
    ferreteriaRenderer = GameObject.Find("BotoneraFerreteria").GetComponent(Renderer);
    ferreteriaCollider = GameObject.Find("BotoneraFerreteria").GetComponent(Collider);
    lacteosRenderer = GameObject.Find("BotoneraLacteos").GetComponent(Renderer);
    lacteosCollider = GameObject.Find("BotoneraLacteos").GetComponent(Collider);
    verduleriaRenderer = GameObject.Find("BotoneraVerduleria").GetComponent(Renderer);
    verduleriaCollider = GameObject.Find("BotoneraVerduleria").GetComponent(Collider);


}

function OnMouseDown(){
    if(GetComponent.<Renderer>().material.mainTexture == texturaGris){ 
     	GetComponent.<Renderer>().material.mainTexture = texturaNaranja;

 		categoriasRenderer.enabled = true;
 		alimentosRenderer.enabled = true;
 		alimentosCollider.enabled = true;
 		bazarRenderer.enabled = true;
 		bazarCollider.enabled = true;
 		bebidasRenderer.enabled = true;
 		bebidasCollider.enabled = true;
 		carniceriaRenderer.enabled = true;
 		carniceriaCollider.enabled = true;
 		ferreteriaRenderer.enabled = true;
 		ferreteriaCollider.enabled = true;
 		lacteosRenderer.enabled = true;
 		lacteosCollider.enabled = true;
 		verduleriaRenderer.enabled = true; 		
 		verduleriaCollider.enabled = true; 		
	}
    else{
		GetComponent.<Renderer>().material.mainTexture = texturaGris;
		
 		categoriasRenderer.enabled = false;
 		alimentosRenderer.enabled = false;
 		alimentosCollider.enabled = false;
 		bazarRenderer.enabled = false;
 		bazarCollider.enabled = false;
 		bebidasRenderer.enabled = false;
 		bebidasCollider.enabled = false;
 		carniceriaRenderer.enabled = false;
 		carniceriaCollider.enabled = false;
 		ferreteriaRenderer.enabled = false;
 		ferreteriaCollider.enabled = false;
 		lacteosRenderer.enabled = false;
 		lacteosCollider.enabled = false;
 		verduleriaRenderer.enabled = false; 		
 		verduleriaCollider.enabled = false; 	
  }
}