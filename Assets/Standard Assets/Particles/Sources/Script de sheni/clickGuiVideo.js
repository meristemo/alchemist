private var objeto:GameObject;
private var shaderOriginal:Shader;
private var shaderNuevo:Shader;
private var direccion:String = "http://www.youtube.com/watch?v=Z8ySfNDLi5w&context=C4537dfcADvjVQa1PpcFMHdq-ryYTBYDZk2b0IbIIkr34lr7wkhZY=";

function Start(){
	objeto = transform.gameObject;
	shaderOriginal = Shader.Find("Transparent/Diffuse");
	shaderNuevo = Shader.Find("Self-Illumin/Diffuse");
	objeto.transform.GetComponent.<Renderer>().material.shader = shaderOriginal;
}

function OnMouseOver(){
	objeto.transform.GetComponent.<Renderer>().material.shader = shaderNuevo;
}

function OnMouseExit(){
	objeto.transform.GetComponent.<Renderer>().material.shader = shaderOriginal;
}

function OnMouseDown(){
	if(Application.isWebPlayer){
		Application.ExternalEval("window.open('http://www.youtube.com/watch?v=Z8ySfNDLi5w&context=C4537dfcADvjVQa1PpcFMHdq-ryYTBYDZk2b0IbIIkr34lr7wkhZY=','','top=300,left=300,width=420,height=315,toolbar=no,scrollbars=yes,Location=no')");
	}
	else{
		Application.OpenURL(direccion);
	}
}