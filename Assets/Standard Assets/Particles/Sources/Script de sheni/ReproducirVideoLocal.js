public var movNavegacion : MovieTexture;
public var movInteractividad : MovieTexture;
public var audioInteractividad : AudioClip;
private var componenteAudioSourse;


function Start () {
    GetComponent.<Renderer>().material.mainTexture = movNavegacion;
	movNavegacion.Play();
}

function videoInteractividad () {
    GetComponent.<Renderer>().material.mainTexture = movInteractividad;
    GetComponent.<Renderer>().GetComponent.<AudioSource>().clip = movInteractividad.audioClip;
	GetComponent.<Renderer>().material.mainTexture.Play();
	GetComponent.<Renderer>().GetComponent.<AudioSource>().Play();

}

function audioApagar () {
	GetComponent.<AudioSource>().Stop();
}

function audioPrender () {
	GetComponent.<AudioSource>().Play();
}