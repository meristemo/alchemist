public var ObjetoAnimado:GameObject;
private var streamScript;
private var videoScript;
private var mostrar;
public var texturaNaranja:Texture;
private var AsistenteTextura:Texture;


function Start () {
	var asistente : GameObject = GameObject.Find("Asistente virtual Navegacion");
	videoScript = asistente.GetComponent("ReproducirVideoLocal");
	var botonAsistente : GameObject = GameObject.Find("BotoneraAsistente");
	AsistenteTextura = botonAsistente.GetComponent.<Renderer>().material.mainTexture;
	//El codigo comentado es para cargar por streaming un video en otro objeto.
	//streamScript = asistente.GetComponent("movieStreamController");
}

function OnTriggerEnter(other:Collider){
	ObjetoAnimado.GetComponent.<Animation>().Play("OfertaAparece");
	if (AsistenteTextura == texturaNaranja) {
		videoScript.videoInteractividad();
	}
	//El codigo comentado es para cargar por streaming un video en otro objeto.
	/*
	streamScript.url = "http://inixiavf.com/sheni/videos/objetos_interactivos.ogv";
	streamScript.playWhenReady = true;
	streamScript.isRendered = false;
	streamScript.LoadVideo();
	*/
}

function OnTriggerExit(other:Collider){
	ObjetoAnimado.GetComponent.<Animation>().Play("OfertaDesaparece");
}
