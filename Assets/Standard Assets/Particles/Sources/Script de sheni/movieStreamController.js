#pragma strict

// The URL to the video we want to play.
//public var url = "http://inixiavf.com/sheni/videos/asistente_virtual.ogv";
public var url = "http://www.nexo-consulting.com/sheni/videos/asistente_virtual.ogv";
public var loop = false;
public var playOnClick = false;			// If true the video will play only when the user clicks this GameObject.
public var playWhenReady : boolean = false;
public var isRendered : boolean = false;

private var wwwData : WWW;
private var movie : MovieTexture;
private var initialTexture : Texture;


// A very dirty fix for the multiple rendering problem.
static var videoGameObjs : ArrayList = new ArrayList();

private class VideoGameObject
{
	public var gameObject : GameObject;
	public var initialTexture : Texture;
	
	public function ToString() {
		return gameObject.ToString();
	}
}

function Start()
{
	//url = "http://inixiavf.com/sheni/videos/asistente_virtual.ogv";
	url = "http://www.nexo-consulting.com/sheni/videos/asistente_virtual.ogv";
	LoadVideo();
}

function LoadVideo()
{
	// Start downloading the given URL.
	wwwData = new WWW(url);
	if (wwwData.error)
		Debug.LogError(wwwData.error);

	movie = wwwData.movie;
	movie.loop = loop;
	
	initialTexture = gameObject.GetComponent.<Renderer>().material.mainTexture;
	Debug.Log("Texture: " + initialTexture);
	
	var newVideo = new VideoGameObject();
	newVideo.gameObject = gameObject;
	newVideo.initialTexture = gameObject.GetComponent.<Renderer>().material.mainTexture;
	videoGameObjs.Add(newVideo);
	Debug.Log("Added " + newVideo.ToString());
}

function Update()
{
	if (!playOnClick || playWhenReady) {
		renderVideo();
		playStreamedVideo();
	}
}

function OnMouseDown()
{
	if (playOnClick)
	{
		renderVideo();
		
		if (!movie.isPlaying && !GetComponent.<AudioSource>().isPlaying) {
			Debug.Log("Attempting to play the video on " + gameObject.ToString());
			playStreamedVideo();
			playWhenReady = true;
			resetMainTexture();
		}
		else {
			Debug.Log("Attempting to pause the video on " + gameObject.ToString());
			pauseStreamedVideo();
			playWhenReady = false;
		}
	}
		
}

public function playStreamedVideo()
{
	// Plays the movie only when there is enough data.
	if (movie.isReadyToPlay && GetComponent.<AudioSource>().clip.isReadyToPlay) {
		Debug.Log("Ready to play: Playing on " + gameObject.ToString());
		if (!movie.isPlaying)
			movie.Play();
		if (!GetComponent.<AudioSource>().isPlaying)
			GetComponent.<AudioSource>().Play();
	}
	else {
		Debug.Log("Do not ready to play: Pausing on " + gameObject.ToString());
		Debug.Log("Progress: " + wwwData.progress);
		movie.Pause();
		GetComponent.<AudioSource>().Pause();
	}
}

public function pauseStreamedVideo()
{
	movie.Pause();
	GetComponent.<AudioSource>().Pause();
	Debug.Log("Paused on " + gameObject.ToString());
}

public function renderVideo()
{
	if (!isRendered) {
		gameObject.GetComponent.<Renderer>().material.mainTexture = movie;
		gameObject.GetComponent.<AudioSource>().clip = movie.audioClip;
		isRendered = true;
	}
}

private function resetMainTexture()
{
	for (var video : VideoGameObject in videoGameObjs) {
		if (!video.gameObject.Equals(gameObject)) {
			Debug.Log("Reseting texture of " + video.gameObject.ToString());
			if (video.gameObject.GetComponent.<Renderer>().material.mainTexture != null) {
				video.gameObject.GetComponent.<Renderer>().material.mainTexture = video.initialTexture;
			}
		}
	}
}

@script RequireComponent(MeshRenderer)
@script RequireComponent(MeshCollider)
@script RequireComponent(AudioSource)