public var camaraAnimada:Camera;
public var camaraPersonaje:Camera;

function Start(){
	camaraPersonaje.GetComponent.<Camera>().enabled = false;
	camaraAnimada.GetComponent.<Camera>().enabled = true;
}

function Update(){
	if(!camaraAnimada.GetComponent.<Animation>().isPlaying){
		camaraAnimada.GetComponent.<Camera>().enabled = false;
		camaraPersonaje.GetComponent.<Camera>().enabled = true;
	}
}