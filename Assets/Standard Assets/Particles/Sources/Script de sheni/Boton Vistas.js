private var objeto:GameObject;
private var MouseLook:GameObject;
private var shaderOriginal:Shader;
private var shaderNuevo:Shader;
public var camara:Camera;
public var player:GameObject;
public var texturaGris:Texture;
public var texturaNaranja:Texture;
public var camaraPlayer:Camera;
//public var opciones:GameObject;
private var brilla: boolean = false;
private var botonVistaCategoriasScript;

function Start(){
	objeto = transform.gameObject;
	//Se crean los shaders: normal y autoiluminado.
	shaderOriginal = Shader.Find("Transparent/Diffuse");
	shaderNuevo = Shader.Find("Self-Illumin/Diffuse");
	var botonVistaCategorias : GameObject = GameObject.Find("BotoneraVistaCategorias");
	botonVistaCategoriasScript = botonVistaCategorias.GetComponent("Boton Vista Categorias");
}
function OnMouseOver(){
    //opciones.GetComponent("Animation").enabled = false;
	//objeto.transform.renderer.material.shader = shaderNuevo;
	  if (!brilla) {
	  	GetComponent.<Animation>().Play("Botonera Brillo Aumenta");
	  	brilla = true;
	  }
}
function OnMouseExit(){
	//objeto.transform.renderer.material.shader = shaderOriginal;
	  if (brilla) {
	  	GetComponent.<Animation>().Play("Botonera Brillo Disminuye");
	  	brilla = false;
	  }
}

function OnMouseDown(){
  //Si la camara esta abajo...
    if(camara.transform.position.y < 1){ 
        //Activo la animacion para que la camara suba
          camara.GetComponent.<Animation>().CrossFade("Camara Sube",0,1);
    	//Le quito a la camara el giro hacia arriba y abajo
     	camara.GetComponent("MouseLook").enabled = false;
     	GetComponent.<Renderer>().material.mainTexture = texturaNaranja;
	}
  //Si la camara esta arriba...	
  else{
     // Activo la animacion para que la camara baje
    camara.GetComponent.<Animation>().CrossFade("Camara Baja",0,1);
     //Le reactivo a la camara el giro hacia arriba y abajo
	camara.GetComponent("MouseLook").enabled = true;
	GetComponent.<Renderer>().material.mainTexture = texturaGris;
   	//Le reactivo a la camara el giro hacia arriba y abajo
     camaraPlayer.GetComponent("MouseLook").enabled = true;
     player.GetComponent("CharacterMotor").enabled = true;
     player.GetComponent("MouseLook").enabled = true;
     botonVistaCategoriasScript.CambiarATexturaGris();
  }
}

