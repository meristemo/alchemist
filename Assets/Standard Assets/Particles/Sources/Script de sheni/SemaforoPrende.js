public var Comercial:boolean;
public var Contacto:boolean;
public var Social:boolean;
public var Video:boolean;

function OnTriggerEnter(other:Collider){
	other.GetComponent("SemaforoPlayer").SendMessage("TieneComercial",Comercial);
	other.GetComponent("SemaforoPlayer").SendMessage("TieneContacto",Contacto);
	other.GetComponent("SemaforoPlayer").SendMessage("TieneVideo",Video);
	other.GetComponent("SemaforoPlayer").SendMessage("TieneSocial",Social);
}