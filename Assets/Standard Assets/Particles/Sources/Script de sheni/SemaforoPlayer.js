var comercial_on:GUITexture;
private var tieneComercial:boolean = false;
var social_on:GUITexture;
private var tieneSocial:boolean = false;
var video_on:GUITexture;
private var tieneVideo:boolean = false;
var contacto_on:GUITexture;
private var tieneContacto:boolean = false;

function TieneContacto(contacto:boolean){
	contacto_on.enabled = contacto;
}
function TieneComercial(comercial:boolean){
	comercial_on.enabled = comercial;
}
function TieneSocial(social:boolean){
	social_on.enabled = social;
}
function TieneVideo(video:boolean){
	video_on.enabled = video;
}