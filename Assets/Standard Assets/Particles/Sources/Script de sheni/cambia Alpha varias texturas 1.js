//Objetos
public var planos:GameObject[];

//Tiempo de Espera entre una textura y otra
public var tiempoEspera:float = 3.0;

//Colores
private var rojo:float = 0.5;
private var verde:float = 0.5;
private var azul:float = 0.5;

//Alpha
private var alphaCreciente:float;
private var alphaDecreciente:float;

//variables de control maximo es el tama�o de la matriz y ultimo es el ultimo indice de la matriz, indice es el indice de la matriz.
private var maximo:int;
private var ultimo:int;
private var indice:int;
private var anterior:int;
private var habilitado:boolean = true;

function Start(){
	maximo = planos.length;
	ultimo = maximo - 1;
	anterior = ultimo;
	indice = 0;
	alphaCreciente = 0.0;
	alphaDecreciente = 1.0;
	for(var j = 0;j<maximo;j++){
		planos[j].GetComponent.<Renderer>().material.color = Color(rojo,verde,azul,0.0);
	}
}

//tengo qe controlar que si est� habilitado haga el cambio de textura, sino no haga nada
function Update(){
	if(habilitado){
		//cambiar valores de alpha
		if(alphaCreciente<1.0){
			alphaCreciente += 0.01;
			alphaDecreciente -= 0.01;
		}
		else{
			Habilita();
		}
	}
	planos[indice].GetComponent.<Renderer>().material.color = Color(rojo,verde,azul,alphaCreciente);
	planos[anterior].GetComponent.<Renderer>().material.color = Color(rojo,verde,azul,alphaDecreciente);
}

function Habilita(){
	habilitado = false;

	yield WaitForSeconds(tiempoEspera);
//0-1-2-3
	alphaCreciente = 0.0;
	alphaDecreciente = 1.0;
	if(indice == ultimo){
		indice = 0;
		anterior = ultimo;
	}
	else{
		indice++;
		anterior = indice-1;
	}
	habilitado = true;

	planos[indice].GetComponent.<Renderer>().material.color = Color(rojo,verde,azul,alphaCreciente);
	planos[anterior].GetComponent.<Renderer>().material.color = Color(rojo,verde,azul,alphaDecreciente);
}