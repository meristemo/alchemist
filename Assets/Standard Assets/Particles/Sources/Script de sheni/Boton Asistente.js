public var texturaGris:Texture;
public var texturaNaranja:Texture;
public var asistenteVirtualRenderer;
private var audioScript;
private var asistenteMarcoRenderer;


function Start(){
	var asistenteVirtual : GameObject = GameObject.Find("Asistente virtual Navegacion");
	asistenteVirtualRenderer = asistenteVirtual.GetComponent("Renderer");
	audioScript = asistenteVirtual.GetComponent("ReproducirVideoLocal");
	var asistenteMarco : GameObject = GameObject.Find("Asistente virtual Navegacion marco");
	asistenteMarcoRenderer = asistenteMarco.GetComponent("Renderer");
}


function OnMouseDown(){

    if(GetComponent.<Renderer>().material.mainTexture == texturaGris){ 
     	GetComponent.<Renderer>().material.mainTexture = texturaNaranja;
     	asistenteMarcoRenderer.enabled = true;
		asistenteVirtualRenderer.enabled = true;
		audioScript.audioPrender ();
	}

    else{

		GetComponent.<Renderer>().material.mainTexture = texturaGris;
     	asistenteVirtualRenderer.enabled = false;
     	asistenteMarcoRenderer.enabled = false;
     	audioScript.audioApagar() ;
  }
}
