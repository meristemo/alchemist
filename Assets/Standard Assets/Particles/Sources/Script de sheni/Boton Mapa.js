public var texturaGris:Texture;
public var texturaNaranja:Texture;
private var botonCamaraEnabled;
public var miniMapaCamara:Camera;
private var mapaMarcoRenderer;


function Start(){
	var mapaMarco : GameObject = GameObject.Find("Minimapa marco");
	mapaMarcoRenderer = mapaMarco.GetComponent("Renderer");
}

function OnMouseDown(){
    if(GetComponent.<Renderer>().material.mainTexture == texturaGris){ 
     	GetComponent.<Renderer>().material.mainTexture = texturaNaranja;
     	miniMapaCamara.enabled = true;
     	mapaMarcoRenderer.enabled = true;
	}
    else{
		GetComponent.<Renderer>().material.mainTexture = texturaGris;
		miniMapaCamara.enabled = false;
		mapaMarcoRenderer.enabled = false;
  }
}
