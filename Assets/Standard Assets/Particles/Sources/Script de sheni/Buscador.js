#pragma strict

public var player : GameObject;
public var searchButtonImage : Texture;
public var textFieldBackgroundImage : Texture;

private var entryToSearch : String = "";
private var objects : Array = null;

// GUI Styles
private var buttonStyle : GUIStyle = new GUIStyle();

function Start ()
{
	// Para mostrar solo la imagen de fondo del boton.
	buttonStyle.imagePosition = ImagePosition.ImageOnly;
}

function OnGUI()
{
	// Esto sirve para agrupar objetos GUI.
	//GUILayout.BeginArea (Rect(Screen.width - 200, 10, 190,400));
	GUILayout.BeginArea (Rect(Screen.width - 366, 5, 190,400));

	GUILayout.BeginVertical();

	GUILayout.BeginHorizontal();

	// Dibujo el boton de busqueda.
	//if (GUILayout.Button(searchButtonImage, buttonStyle))
		//objects = Search(entryToSearch);
	GUILayout.Button(searchButtonImage, buttonStyle);
	
	// Dibujo el campo de texto.
	var oldEntry : String = entryToSearch;
	entryToSearch = GUILayout.TextField(entryToSearch, 50, GUILayout.Width(150));
	
	if (entryToSearch != oldEntry)
		objects = Search(entryToSearch);

	GUILayout.EndHorizontal();

	// Muestro los resultados de la busqueda.
	if (objects != null)
	{
		GUILayout.BeginVertical();
		
		for (var i:uint=0; i<objects.length; i++)
		{
			var obj : GameObject = objects[i];
			// If the user selects an article in the list.

			if (GUILayout.Button(obj.name, GUILayout.Width(170))) {
				TeletransportTo(obj);
				entryToSearch = "";
				objects.Clear();
				GUIUtility.keyboardControl = 0;
			}
		}

		GUILayout.EndVertical();
	}
	
	GUILayout.EndVertical();
	GUILayout.EndArea();
}

// Busca de entre todos los gameObjects con Tags "Articulo" y "Zona"
// y muestra los objetos que tienen nombre similar a "entry".
// Ordena los resultados segun proximidad.
function Search(entry : String) : Array
{
	var result : Array = new Array();
	Debug.Log("Searching for ... " + entry);

	if (entry == "")
		return result;

	var articles : GameObject[] = GameObject.FindGameObjectsWithTag("Articulo");
	var zonas : GameObject[] = GameObject.FindGameObjectsWithTag("Zona");

	var objects : Array = new Array();
	objects.AddRange(articles);
	objects.AddRange(zonas);

	var matchPrefix : Array = new Array();

	// Filtro los GameObjects que tienen un nombre parecido a entry.
	// Separo por un lado los que tienen como prefijo a entry.
	for (var i:uint=0; i<objects.length; i++)
	{
		var obj : GameObject = objects[i];
		if (obj.name.ToLower().Contains(entry.ToLower()))
		{
			if (obj.name.ToLower().StartsWith(entry.ToLower()))
				matchPrefix.Add(obj);
			else
				result.Add(obj);
		}
	}
	
	result = matchPrefix.Concat(result);

	Debug.Log("Results: " + result);

	return result;
}

// Mueve al player hasta encontrarse cerca de un game object.
function TeletransportTo(obj : GameObject) : void
{
	var zonaDestino : GameObject[] = GameObject.FindGameObjectsWithTag("ZonaDestino");

	var zonaMasCercana : GameObject = zonaDestino[0];

	for (var i:uint=0; i<zonaDestino.length; i++)
	{
		var zPos1 = zonaDestino[i].transform.position;
		var zPos2 = zonaMasCercana.transform.position;
		var objPos = obj.transform.position;
		if (Vector3.Distance(zPos1, objPos) < Vector3.Distance(zPos2, objPos))
			zonaMasCercana = zonaDestino[i];
	}

	var vec1 : Vector3 = zonaMasCercana.transform.position;
	var vec2 : Vector3 = obj.transform.position;
	var dif : Vector3 = vec2 - vec1;
	dif.y = 0.0;
	player.transform.position = vec1;
	player.transform.Translate(dif / 2.0, Space.World);
	player.transform.LookAt(obj.transform);
	player.transform.rotation.z = 0;
	player.transform.rotation.x = 0;
}