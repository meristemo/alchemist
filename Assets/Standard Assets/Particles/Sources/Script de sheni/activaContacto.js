

//correo al que se va a mandar el email de contacto.
public var correo:String;
//es el que llama al player
private var objeto:GameObject;
//componente: el player tiene un script que se llama Formulario, dentro de ese script est� el codigo para el formulario,
//lo que esto hace es llamar a se componente para "crear" el formulario.
private var componente;
//Cuando se inicia el script, llama al player y lo caga en objeto, busca el componente formulario y lo carga en componente.
function Start(){
	objeto = new GameObject.Find("Player");
	componente = objeto.GetComponent(FormularioDeRegistro);
}
//Cuando se hace click al objeto que tiene ESTE script cargado pasa lo siguiente:
/*
function OnMouseDown () {
	//A la "propiedad" (variable) correoStand, dentro del componente Formulario del Player, le asigna e correo que definimos antes.
	componente.correoStand = correo;
	//Al "estado" (variable) prendido, le da valor verdadero para que muestre el correo.
	componente.prendido = true;
}
*/
function OnTriggerEnter(other:Collider){
	componente.correoStand = correo;
	//Al "estado" (variable) prendido, le da valor verdadero para que muestre el correo.
	componente.prendido = true;
}

function OnTriggerExit(other:Collider){
	componente.correoStand = correo;
	//Al "estado" (variable) prendido, le da valor verdadero para que muestre el correo.
	componente.prendido = false;
}