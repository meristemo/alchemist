﻿#pragma strict

import UnityEngine.UI;

var mySlider : UnityEngine.UI.Slider;
var score : Text;
static var temp : int;

public var font : Font;

function Start () {
	mySlider = GameObject.Find("Slider").GetComponent(UnityEngine.UI.Slider);
	temp = 0;
}

function Update () {
	mySlider.value = temp;
	score.text = temp.ToString();
}