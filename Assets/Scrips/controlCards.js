﻿#pragma strict

import UnityEngine.UI;
public var card : int;
public var anim : Animator;
public var anim_bruja : Animator;
var card_1 : GameObject;
var card_2 : GameObject;
var card_3 : GameObject;
var cartaBruja : GameObject;
public var choosenCard : int;
static var cards = new Array ();
public var rand_pos : int;
public var first_card : int;
public var second_card : int;
public var third_card : int;
public var enemy_first_card : int;
public var enemy_second_card : int;
public var enemy_third_card : int;
static var isWaiting : boolean;

public var sol:Texture;
public var fuego:Texture;
public var madera:Texture;
public var viento:Texture;
public var piedra:Texture;
public var hielo:Texture;
public var agua:Texture;
public var tierra:Texture;
public var metal:Texture;

public var replace_card_enemy : int;

function Start () {
	initGame();
	anim= GetComponent("Animator");
	cartaBruja = new GameObject.Find("cartaBruja");
	anim_bruja = cartaBruja.GetComponent("Animator");
	enemy_first_card = cards.Shift();
	enemy_second_card = cards.Shift();
	enemy_third_card = cards.Shift();

	first_card = cards.Shift();
	second_card = cards.Shift();
	third_card = cards.Shift();

	if(card == 1) {
		defineCard(first_card);
	} else if (card == 2) {
		defineCard(second_card);
	} else {
		defineCard(third_card);
	}

	isWaiting = false;
}

static function initGame() {
	cards = [50, 30, 30, 30, 20, 20, 20, 20, 10, 10, 5, 5, 0, 0, -10, -10, -20, -20, -30, 50, 30, 30, 30, 20, 20, 20, 20, 10, 10, 5, 5, 0, 0, -10, -10, -20, -20, -30];
	RandomizeArray(cards);

}

function Update () {

}


function OnMouseOver() {
	if(controlSlider.temp < 100 && !isWaiting) {
		if(Input.GetMouseButtonDown(0)) {
			isWaiting = true;
			controlGame.ganoJugador=false;
	         switch(card) {
	         	case(1):
	         		controlSlider.temp += first_card;
	         		anim.SetTrigger("alCaldero");
	         		anim.SetTrigger("new_card");
	         		replaceCard(1);
	         		yield WaitForSeconds(1);
	         		defineCard(first_card);
	         		break;
	         	case(2):
	         		controlSlider.temp += second_card;
	         		anim.SetTrigger("alCaldero");
	         		anim.SetTrigger("new_card");
	         		replaceCard(2);
	         		yield WaitForSeconds(1);
	         		defineCard(second_card);
	         		break;
	         	case(3):
	         		controlSlider.temp += third_card;
	         		anim.SetTrigger("alCaldero");
	         		anim.SetTrigger("new_card");
	         		replaceCard(3);
	         		yield WaitForSeconds(1);
	         		defineCard(third_card);
	         		break;
	         }

	         yield WaitForSeconds (0.5);

	         var replace_card_enemy = getBestCard(enemy_first_card, enemy_second_card, enemy_third_card, controlSlider.temp);
         	 defineCardEnemy(choosenCard);
	         controlSlider.temp += choosenCard;

	         anim_bruja.SetTrigger("bolaVoladora");
		 	 replaceCardEnemy(replace_card_enemy);
		 	 controlGame.ganoJugador = true;
	         

	         yield WaitForSeconds (1.0);

	         isWaiting = false;
	     }
	}
}

static function RandomizeArray(arr : Array)
{
    for (var i = arr.length - 1; i > 0; i--) {
        var r = Random.Range(0,i);
        var tmp = arr[i];
        arr[i] = arr[r];
        arr[r] = tmp;
    }
}

function replaceCard(nrCard : int) {
	switch(nrCard) {
		case(1):
			first_card = cards.Shift();
			break;
		case(2):
			second_card = cards.Shift();
			break;
		case(3):
			third_card = cards.Shift();
			break;
	}
}

function replaceCardEnemy(nrCard : int) {
	switch(nrCard) {
		case(1):
			//Debug.Log("Se reeemplazo la primer carta");
			enemy_first_card = cards.Shift();
			break;
		case(2):
			//Debug.Log("Se reeemplazo la segunda carta");
			enemy_second_card = cards.Shift();
			break;
		case(3):
			//Debug.Log("Se reeemplazo la tercer carta");
			enemy_third_card = cards.Shift();
			break;
	}
}

function defineCard( value : int ) {
	switch(value) {
		case(50):
			 GetComponent.<Renderer>().material.mainTexture = sol;
			 break;
		case(30):
			 GetComponent.<Renderer>().material.mainTexture = fuego;
			 break;
		case(20):
			 GetComponent.<Renderer>().material.mainTexture = madera;
			 break;
		case(10):
			 GetComponent.<Renderer>().material.mainTexture = viento;
			 break;
		case(5):
			 GetComponent.<Renderer>().material.mainTexture = metal;
			 break;
		case(0):
			 GetComponent.<Renderer>().material.mainTexture = piedra;
			 break;
		case(-10):
			 GetComponent.<Renderer>().material.mainTexture = tierra;
			 break;
		case(-20):
			 GetComponent.<Renderer>().material.mainTexture = agua;
			 break;
		case(-30):
			 GetComponent.<Renderer>().material.mainTexture = hielo;
			 break;
	}
}

function defineCardEnemy( value : int ) {
	cartaBruja = new GameObject.Find("cartaBruja");
	switch(value) {
		case(50):
			 cartaBruja.GetComponent.<Renderer>().material.mainTexture = sol;
			 break;
		case(30):
			 cartaBruja.GetComponent.<Renderer>().material.mainTexture = fuego;
			 break;
		case(20):
			 cartaBruja.GetComponent.<Renderer>().material.mainTexture = madera;
			 break;
		case(10):
			 cartaBruja.GetComponent.<Renderer>().material.mainTexture = viento;
			 break;
		case(5):
			 cartaBruja.GetComponent.<Renderer>().material.mainTexture = metal;
			 break;
		case(0):
			 cartaBruja.GetComponent.<Renderer>().material.mainTexture = piedra;
			 break;
		case(-10):
			 cartaBruja.GetComponent.<Renderer>().material.mainTexture = tierra;
			 break;
		case(-20):
			 cartaBruja.GetComponent.<Renderer>().material.mainTexture = agua;
			 break;
		case(-30):
			 cartaBruja.GetComponent.<Renderer>().material.mainTexture = hielo;
			 break;
	}
}

public function getBestCard(efc : int, esc: int, etc : int, temp : int) {
	var val_return : int;
	if((efc + temp) < 100) {
		val_return = 1;
		choosenCard = efc;
	} else if((efc + temp) < 100) {
		val_return = 2;
		choosenCard = esc;
	} else {
		val_return = 3;
		choosenCard = etc;
	}
	return val_return;
}


