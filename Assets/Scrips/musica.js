@script RequireComponent(AudioSource)

private var randomize : int;
private var pasadas : int;
public var score : int;
private var lastKeyPressed;
private var lastKeyPressedAnt;
private var sequence = new Array("left","right");
private var lanzero : GameObject;
private var explorador : GameObject;
private var tambores1 : GameObject;
private var tambores2 : GameObject;
private var tambores3 : GameObject;
private var pinchazos : int;
private var tiempo : float;
private var tiempo_stop : float;
private var fuego : GameObject;
private var perdiste : int;
private var doing : GameObject;
private var sonido_perdiste : GameObject;
private var GameOverAparece : GameObject;



function Start() {
    GetComponent.<AudioSource>().Play();
    pasadas = 0;
    score = 0;
    lanzero = new GameObject.Find("CanibalitoLancero_Animaciones");
    explorador = new GameObject.Find("Explorador");
    tambores1 = new GameObject.Find("CanibalitoTamborilero1_Animaciones");
    tambores2 = new GameObject.Find("CanibalitoTamborilero2_Animaciones");
    tambores3 = new GameObject.Find("CanibalitoTamborilero3_Animaciones");
    GameOverAparece = new GameObject.Find("GameOver");
    fuego = new GameObject.Find("FuegoCaldero");
    doing = new GameObject.Find("Doing");
    sonido_perdiste = new GameObject.Find("AlegriaCanibales");
    pinchazos = 0;
    tiempo = 0.0;
    tiempo_stop = 0.0;
    perdiste = 0;
}

function Update()	{
	randomize = Random.Range(1,100);
	pasadas++;
	tiempo = (1 * Time.deltaTime) + tiempo;
	//print(tiempo);
	if(GetComponent.<AudioSource>().isPlaying)	{
		for (i=0;i<sequence.length;i++)
  		{
  			if(i == 0)	{
  				valLast = sequence.length - 1;
  				lastKeyPressedAnt = sequence[valLast];
	       	}
	       	else 	{
	       		lastKeyPressedAnt = sequence[i-1];
	       	}
	       	
  			if(Input.GetKeyDown (sequence[i])) {
  				if((lastKeyPressedAnt == lastKeyPressed) || lastKeyPressed == null)	{
       				score++;
					lastKeyPressed = sequence[i];
				}
			}
		
			if((randomize == 50 && pasadas >= 50) || pasadas > 750)	{
				GetComponent.<AudioSource>().Stop();
				tiempo_stop = tiempo;
				tambores1.GetComponent.<Animation>().CrossFade("stop",0.2);
				tambores2.GetComponent.<Animation>().CrossFade("stop",0.2);
				tambores3.GetComponent.<Animation>().CrossFade("stop",0.2);
				pasadas = 0;
			}
		}
	}
	else
	{
		if(!explorador.GetComponent.<Animation>().IsPlaying("salto"))	{
			explorador.GetComponent.<Animation>().CrossFade("descansando",0.2);
		}
		for (i=0;i<sequence.length;i++)
  		{
  			if(Input.GetKeyDown (sequence[i])) {
  				if(tiempo >= (tiempo_stop + 0.4))	{
	  		    	pinchazos++;
	  		    	if(pinchazos < 6)	{
		  		    	score = score - 25;
		  		    	doing.GetComponent.<AudioSource>().Play();
				        lanzero.GetComponent.<Animation>().CrossFade("lanzaso",0.2);
				        lanzero.transform.position.x = lanzero.transform.position.x + -0.2;
				        explorador.transform.position.x = explorador.transform.position.x + -0.2;
				        explorador.GetComponent.<Animation>().CrossFade("salto",0.2);
					}
					else if(pinchazos == 6)	{
						score = score - 25;
						doing.GetComponent.<AudioSource>().Play();
						lanzero.GetComponent.<Animation>().CrossFade("lanzaso",0.2);
						explorador.GetComponent.<Animation>().CrossFade("salto",0.2);
						lanzero.transform.position.x = lanzero.transform.position.x + -0.2;
						explorador.transform.position.x = explorador.transform.position.x + -0.4;
						explorador.transform.position.z = explorador.transform.position.z - 0.2;
						sonido_perdiste.GetComponent.<AudioSource>().Play();
						print("Perdiste");
				        perdiste = 1;
				        GameOverAparece.GetComponent.<Animation>().Play("GameOverAparece");
				    }
				    print(pinchazos);
				}
			}
		}
		if((randomize == 50 && pasadas >= 50) || pasadas > 500)	{
			if(pinchazos < 6)	{
				GetComponent.<AudioSource>().Play();
				tambores1.GetComponent.<Animation>().CrossFade("idle",0.2);
				tambores2.GetComponent.<Animation>().CrossFade("idle",0.2);
				tambores3.GetComponent.<Animation>().CrossFade("idle",0.2);
				explorador.GetComponent.<Animation>().CrossFade("vailando",0.2);
				pasadas = 0;
			}
		}
	}
	
	if(!lanzero.GetComponent.<Animation>().IsPlaying("lanzaso"))	{
		lanzero.GetComponent.<Animation>().CrossFade("idle",0.2);
	}
	
	// Disminuir fuego
}

//OnGUI is called multiple times per frame. Use this for GUI stuff only!
function OnGUI()
{
    //We display the game GUI from the playerscript
    //It would be nicer to have a seperate script dedicated to the GUI though...
    GUILayout.Label("Score: " + score);
    if(perdiste == 1)	{
    	//GUILayout.Label("Pediste");
    }
}    
