private var sujetador02:GameObject;
private var sujetador03:GameObject;
private var sujetador04:GameObject;
private var sujetador05:GameObject;
private var sujetador06:GameObject;
private var cortina:GameObject;
private var cortinacerrada:boolean = true;

function Start () {
sujetador02 = new GameObject.Find("Cortina living izquieda sujetador 02");
sujetador03 = new GameObject.Find("Cortina living izquieda sujetador 03");
sujetador04 = new GameObject.Find("Cortina living izquieda sujetador 04");
sujetador05 = new GameObject.Find("Cortina living izquieda sujetador 05");
sujetador06 = new GameObject.Find("Cortina living izquieda sujetador 06");
cortina = new GameObject.Find("Cortina living izquierda");
}

function OnMouseDown(){
	if (cortinacerrada) {
		cortina.GetComponent.<Animation>().Play("Cortina mantener movimiento");
		cortinacerrada = false;
		sujetador02.GetComponent.<Animation>().Play("Cortina living izquieda sujetador ABRE 02");
		sujetador03.GetComponent.<Animation>().Play("Cortina living izquieda sujetador ABRE 03");
		sujetador04.GetComponent.<Animation>().Play("Cortina living izquieda sujetador ABRE 04");
		sujetador05.GetComponent.<Animation>().Play("Cortina living izquieda sujetador ABRE 05");
		sujetador06.GetComponent.<Animation>().Play("Cortina living izquieda sujetador ABRE 06");
	}
	else if (!cortinacerrada) {
		cortina.GetComponent.<Animation>().Play("Cortina mantener movimiento");
		cortinacerrada = true;
		sujetador02.GetComponent.<Animation>().Play("Cortina living izquieda sujetador CIERRA 02");
		sujetador03.GetComponent.<Animation>().Play("Cortina living izquieda sujetador CIERRA 03");
		sujetador04.GetComponent.<Animation>().Play("Cortina living izquieda sujetador CIERRA 04");
		sujetador05.GetComponent.<Animation>().Play("Cortina living izquieda sujetador CIERRA 05");
		sujetador06.GetComponent.<Animation>().Play("Cortina living izquieda sujetador CIERRA 06");
	}

}