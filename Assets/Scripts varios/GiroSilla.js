private var Rueda_Atr_Der : GameObject;
private var Rueda_Atr_Izq : GameObject;
private var Rueda_Ade_Der : GameObject;
private var Rueda_Ade_Izq : GameObject;

private var objeto : GameObject;

function Start () {
   objeto = new GameObject.Find("Player");
   Rueda_Atr_Der = new GameObject.Find("Rueda_Atr_Der");
   Rueda_Atr_Izq = new GameObject.Find("Rueda_Atr_Izq");
   Rueda_Ade_Der = new GameObject.Find("Rueda_Ade_Der");
   Rueda_Ade_Izq = new GameObject.Find("Rueda_Ade_Izq");
}

function Update () {

//var controller : CharacterController = GetComponent(CharacterController);
   
   //Rotate around y - axis
   objeto.transform.Rotate(0, Input.GetAxis ("Horizontal") * 2, 0);
   
 
 
 if (Input.GetKey ("up") || Input.GetKey ("w")) {     

    Rueda_Atr_Der.transform.Rotate(-500* Time.deltaTime, 0,0);
    Rueda_Atr_Izq.transform.Rotate(-500* Time.deltaTime, 0,0);
    Rueda_Ade_Der.transform.Rotate(-500* Time.deltaTime, 0,0);
    Rueda_Ade_Izq.transform.Rotate(-500* Time.deltaTime, 0,0);
}
 
 
  if (Input.GetKey ("down") || Input.GetKey ("s")) {     

    Rueda_Atr_Der.transform.Rotate(500* Time.deltaTime, 0,0);
    Rueda_Atr_Izq.transform.Rotate(500* Time.deltaTime, 0,0);
    Rueda_Ade_Der.transform.Rotate(500* Time.deltaTime, 0,0);
    Rueda_Ade_Izq.transform.Rotate(500* Time.deltaTime, 0,0);
}
 
   if (Input.GetKey ("left") || Input.GetKey ("a")) {     

    Rueda_Atr_Der.transform.Rotate(-350* Time.deltaTime, 0,0);
    Rueda_Atr_Izq.transform.Rotate(350* Time.deltaTime, 0,0);
    Rueda_Ade_Der.transform.Rotate(-350* Time.deltaTime, 0,0);
    Rueda_Ade_Izq.transform.Rotate(350* Time.deltaTime, 0,0);
}
 
    if (Input.GetKey ("right") || Input.GetKey ("d")) {     

    Rueda_Atr_Der.transform.Rotate(350* Time.deltaTime, 0,0);
    Rueda_Atr_Izq.transform.Rotate(-350* Time.deltaTime, 0,0);
    Rueda_Ade_Der.transform.Rotate(350* Time.deltaTime, 0,0);
    Rueda_Ade_Izq.transform.Rotate(-350* Time.deltaTime, 0,0);
}
 
}



